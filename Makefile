CFLAGS+=-fopenmp -O3 -g -march=native
CFLAGS+=-I/home/kfeng/install/include
CFLAGS+=-I/home/kfeng/pkg_src/hiredis
CFLAGS+=-Wl,-rpath=/home/kfeng/netcdf-C/lib
LDFLAGS+=-L/home/kfeng/netcdf-C/lib -lnetcdf
LDFLAGS+=-L/home/kfeng/install/lib -lpthread -lcrypto -lm
LDFLAGS+=-L/home/kfeng/pkg_src/hiredis -lhiredis
#LDFLAGS+=-L/home/kfeng/install/lib -lpthread -lcrypto -llmpe -lmpe -ltmpe -lm
MPICC=/home/kfeng/MPICH/bin/mpicc
CC=gcc
PWD=$(shell pwd)

all: integrator

integrator: integrator.o posix_io.o redis_io.o debug.o debug.h
	$(MPICC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

%.o: %.c
	$(MPICC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf *.o integrator
