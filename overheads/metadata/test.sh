#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

N_REQS_PER_PROC=(16 32 64 128 256 512 1024)
REP=1
EXE_BIN=/mnt/common/kfeng/Integrator/build/test/niobe_overhead_metadata

for n_req in ${N_REQS_PER_PROC[@]}
do
  for rep in `seq 1 $REP`
  do
    echo mpiexec -n 1120 client_nprocs -prepend-rank -f hosts -iface enp47s0 $EXE_BIN 40 $n_req
    mpiexec -n 1120 -prepend-rank -f hosts -iface enp47s0 $EXE_BIN 40 $n_req
    sleep 10
  done
done
