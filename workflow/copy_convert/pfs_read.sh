#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

N_APP_HOSTS=28
APP_PPN=(40)
CLIENTS=clients
SERVERS=servers
REP=5

for app_ppn in ${APP_PPN[@]}
do
  truncate -s 0 hosts
  for i in `seq -w 05 32`
  do
    echo ares-comp-$i-40g:$app_ppn >> hosts
  done
  cat hosts
  ((app_nprocs=$app_ppn*$N_APP_HOSTS))
  ((total_size=$app_nprocs*16))
  echo "Testing $app_nprocs reading ${total_size}MB from PFS using MPI Independent I/O interface ..."
  for rep in `seq 1 $REP`
  do
    echo mpiexec -n $app_nprocs -prepend-rank -f hosts ./pfs_read $app_nprocs 2>&1 | grep -v PVFS
    mpiexec -n $app_nprocs -prepend-rank -f hosts ./pfs_read $app_nprocs 2>&1 | grep -v PVFS
    mpssh -f $CLIENTS 'sudo fm'
    mpssh -f $SERVERS 'sudo fm'
    sleep 2
  done
done
