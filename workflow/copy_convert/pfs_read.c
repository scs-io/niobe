// Author: Wes Kendall
// Copyright 2011 www.mpitutorial.com
// This code is provided freely with the tutorials on mpitutorial.com. Feel
// free to modify it for your own use. Any distribution of the code must
// either provide a link to www.mpitutorial.com or keep this header intact.
//
// MPI_Send, MPI_Recv example. Communicates the number -1 from process 0
// to process 1.
//
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>

#define MEGA (1024 * 1024)
#define BUF_SIZE_IN_MB 4
#define FILE_NAME_BASE "/mnt/nvme/kfeng/pvfs2-mount/data"

int main(int argc, char** argv) {
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);
  // Find out rank, size
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  int i;
  int *buf;
  double start, end, duration, sum_duration, ave_duration;
  int n_app_procs;
  int ratio;
  MPI_File fh;
  MPI_Offset offset;

  if (rank == 0) {
    if (argc != 2) {
      fprintf(stderr, "Usage: %s APP_NPROCS\n", argv[0]);
      exit(-1);
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  n_app_procs = atoi(argv[1]);
  ratio = n_app_procs / size;
  buf = (int *)malloc(MEGA * BUF_SIZE_IN_MB * sizeof(MPI_INT) * ratio);
  memset(buf, 1, MEGA * BUF_SIZE_IN_MB * sizeof(MPI_INT) * ratio);

  start = MPI_Wtime();
  MPI_File_open(MPI_COMM_WORLD, FILE_NAME_BASE,
                MPI_MODE_RDWR | MPI_MODE_CREATE,
                MPI_INFO_NULL, &fh);
  offset = (MPI_Offset) BUF_SIZE_IN_MB * rank * MEGA * sizeof(MPI_INT) * ratio;
  /*printf("Reading %d MPI_INT at offset %ld\n", MEGA * BUF_SIZE_IN_MB * ratio, offset);*/
  MPI_File_read_at(fh, offset,
                  buf, MEGA * BUF_SIZE_IN_MB * ratio, MPI_INT,
                  MPI_STATUS_IGNORE);
  MPI_File_close(&fh);
  end = MPI_Wtime();
  duration = end - start;
  MPI_Reduce(&duration, &sum_duration, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    ave_duration = sum_duration / n_app_procs;
    printf("Time: %lf seconds, aggregate bandwidth is %lf MB/s\n",
        ave_duration,
        BUF_SIZE_IN_MB * n_app_procs * sizeof(MPI_INT) / ave_duration);
  }

  if(buf) free(buf);
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}
