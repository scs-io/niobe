#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

N_APP_HOSTS=28
N_AGGR_HOSTS=(1 2 4)
APP_PPN=(10 20 40)
AGGR_PPN=(20 40)
MODES=("POSIX" "MPI_IND_IO" "MPI_COLL_IO")
CLIENTS=clients
SERVERS=servers
REP=5

for mode in ${MODES[@]}
do
  for n_aggr_hosts in ${N_AGGR_HOSTS[@]}
  do
    for aggr_ppn in ${AGGR_PPN[@]}
    do
      for app_ppn in ${APP_PPN[@]}
      do
        truncate -s 0 hosts
        for i in `seq -w 1 $n_aggr_hosts`
        do
          ((id=$i+$N_APP_HOSTS))
          echo ares-comp-$id-40g:$aggr_ppn >> hosts
        done
        cat hosts
        ((app_nprocs=$app_ppn*$N_APP_HOSTS))
        ((aggr_nprocs=$aggr_ppn*$n_aggr_hosts))
        ((total_size=$app_nprocs*16))
        ((total_size_per_node=$app_nprocs*16/$n_aggr_hosts))
        if [[ $total_size_per_node -gt 40960 ]]
        then
          echo "Too much data (${total_size_per_node}MB) for one aggregator node, skipping ..."
          continue
        fi
        echo "Testing $aggr_nprocs on $n_aggr_hosts nodes flushing ${total_size}MB to PFS using ${mode} interface ..."
        for rep in `seq 1 $REP`
        do
          ((res=$app_nprocs%$aggr_nprocs))
          if [[ $res -ne 0 ]]
          then
            echo application nprocs $app_nprocs cannot be dividedby aggregator nprocs $aggr_nprocs
            echo [0] Time: 999 seconds, aggregate bandwidth is 0 MB/s
            continue
          fi
          echo mpiexec -n $aggr_nprocs -prepend-rank -f hosts ./pfs_write $app_nprocs $aggr_nprocs $n_aggr_hosts $mode
          mpiexec -n $aggr_nprocs -prepend-rank -f hosts ./pfs_write $app_nprocs $aggr_nprocs $n_aggr_hosts $mode
          mpssh -f $CLIENTS 'sudo fm'
          mpssh -f $SERVERS 'sudo fm'
          sleep 2
        done
      done
    done
  done
done
