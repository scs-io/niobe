// Author: Wes Kendall
// Copyright 2011 www.mpitutorial.com
// This code is provided freely with the tutorials on mpitutorial.com. Feel
// free to modify it for your own use. Any distribution of the code must
// either provide a link to www.mpitutorial.com or keep this header intact.
//
// MPI_Send, MPI_Recv example. Communicates the number -1 from process 0
// to process 1.
//
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>

#define MEGA (1024 * 1024)
#define BUF_SIZE_IN_MB 4
#define WRITE_SIZE_IN_MB 14
#define FILE_NAME_BASE "/mnt/nvme/kfeng/pvfs2-mount/data"

int main(int argc, char** argv) {
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);
  // Find out rank, size
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  int i;
  int *buf;
  double start, end, duration, sum_duration, ave_duration;
  int n_app_procs, n_aggr_procs;
  int n_aggr_nodes;
  int n_senders;
  int n_round;
  char *mode;
  char file_name[PATH_MAX];
  FILE *fp;
  MPI_File fh;
  MPI_Offset offset;

  if (rank == 0) {
    if (argc != 5) {
      fprintf(stderr, "Usage: %s \
              APP_NPROCS AGGR_NPROCS AGGR_NNODES MODE\n", argv[0]);
      exit(-1);
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  n_app_procs = atoi(argv[1]);
  n_aggr_procs = atoi(argv[2]);
  n_aggr_nodes = atoi(argv[3]);
  mode = argv[4];
  if (rank == 0) {
    if (n_app_procs % n_aggr_procs != 0) {
      fprintf(stderr, "Error: application nprocs is not \
              multiple of aggregator nprocs\n");
      exit(-1);
    }
  }

  n_senders = n_app_procs / n_aggr_procs;
  buf = (int *)malloc(MEGA * BUF_SIZE_IN_MB * sizeof(MPI_INT) * n_senders);
  memset(buf, 1, MEGA * BUF_SIZE_IN_MB * sizeof(MPI_INT) * n_senders);

  start = MPI_Wtime();
  if (!strcmp(mode, "POSIX")) {
    sprintf(file_name, "%s_%03d", FILE_NAME_BASE, rank);
    if ((fp = fopen(file_name, "w")) == 0) {
      perror("Cannot open file");
      exit(-1);
    }
    n_round = BUF_SIZE_IN_MB * n_senders / WRITE_SIZE_IN_MB;
    /*printf("writing to %s in %d rounds using POSIX interface\n",*/
           /*file_name, n_round);*/
    for (i = 0; i < n_round; i++) {
      fwrite(buf + WRITE_SIZE_IN_MB * MEGA,
             (size_t) sizeof(MPI_INT),
             MEGA * WRITE_SIZE_IN_MB, fp);
    }

    fclose(fp);
  } else {
    MPI_File_open(MPI_COMM_WORLD, FILE_NAME_BASE,
                  MPI_MODE_RDWR | MPI_MODE_CREATE,
                  MPI_INFO_NULL, &fh);
    offset = (MPI_Offset) BUF_SIZE_IN_MB * n_senders * rank
                          * MEGA * sizeof(MPI_INT);
    if (!strcmp(mode, "MPI_IND_IO")) {
      /*printf("writing to %s in %d rounds using MPI independent I/O interface\n",*/
           /*file_name, n_round);*/
      MPI_File_write_at(fh, offset,
                        buf, MEGA * BUF_SIZE_IN_MB * n_senders, MPI_INT,
                        MPI_STATUS_IGNORE);
    } else if (!strcmp(mode, "MPI_COLL_IO")) {
      /*printf("writing to %s in %d rounds using MPI collective I/O interface\n",*/
           /*file_name, n_round);*/
      MPI_File_write_at_all(fh, offset,
                            buf, MEGA * BUF_SIZE_IN_MB * n_senders, MPI_INT,
                            MPI_STATUS_IGNORE);
    }
    MPI_File_close(&fh);
  }
  end = MPI_Wtime();
  duration = end - start;
  MPI_Reduce(&duration, &sum_duration, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    ave_duration = sum_duration / n_aggr_procs;
    printf("Time: %lf seconds, aggregate bandwidth is %lf MB/s\n",
           ave_duration,
           BUF_SIZE_IN_MB * n_app_procs * sizeof(MPI_INT) / ave_duration);
  }

  if(buf) free(buf);
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}
