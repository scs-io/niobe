#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

N_APP_HOSTS=28
N_AGGR_HOSTS=(1 2 4)
APP_PPN=(10 20 40)
AGGR_PPN=(20 40)
REP=5

for n_aggr_hosts in ${N_AGGR_HOSTS[@]}
do
  for aggr_ppn in ${AGGR_PPN[@]}
  do
    for app_ppn in ${APP_PPN[@]}
    do
      truncate -s 0 hosts
      for i in `seq -w 1 $N_APP_HOSTS`
      do
        echo ares-comp-$i-40g:$app_ppn >> hosts
      done
      for i in `seq -w 1 $n_aggr_hosts`
      do
        ((id=$i+$N_APP_HOSTS))
        echo ares-comp-$id-40g:$aggr_ppn >> hosts
      done
      cat hosts
      ((app_nprocs=$app_ppn*$N_APP_HOSTS))
      ((aggr_nprocs=$aggr_ppn*$n_aggr_hosts))
      ((total_nprocs=$app_nprocs+$aggr_nprocs))
      ((total_size=$app_nprocs*16))
      ((res=$app_nprocs%$aggr_nprocs))
      if [[ $res -ne 0 ]]
      then
        echo [0] Time: 999 seconds, aggregate bandwidth is 0 MB/s
        continue
      fi
      if [[ $total_size -gt 40960 ]]
      then
        echo "Too much data (${total_size}MB) for $n_aggr_hosts nodes, skipping ..."
        continue
      fi
      echo "Testing $app_nprocs on $N_APP_HOSTS nodes sending ${total_size}MB to $aggr_nprocs on $n_aggr_hosts nodes ..."
      for rep in `seq 1 $REP`
      do
        echo mpiexec -n $total_nprocs -prepend-rank -f hosts ./max_bw $app_nprocs $N_APP_HOSTS $aggr_nprocs $n_aggr_hosts
        mpiexec -n $total_nprocs -prepend-rank -f hosts ./max_bw $app_nprocs $N_APP_HOSTS $aggr_nprocs $n_aggr_hosts
        sleep 2
      done
    done
  done
done
