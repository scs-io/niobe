// Author: Wes Kendall
// Copyright 2011 www.mpitutorial.com
// This code is provided freely with the tutorials on mpitutorial.com. Feel
// free to modify it for your own use. Any distribution of the code must
// either provide a link to www.mpitutorial.com or keep this header intact.
//
// MPI_Send, MPI_Recv example. Communicates the number -1 from process 0
// to process 1.
//
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MEGA (1024 * 1024)
#define BUF_SIZE_IN_MB 4

int main(int argc, char** argv) {
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);
  // Find out rank, size
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  int i;
  int *buf;
  double start, end, duration, sum_duration, ave_duration;
  int n_app_procs, n_aggr_procs;
  int n_app_nodes, n_aggr_nodes;
  int n_senders;
  int dest_rank, src_rank;
  MPI_Request *reqs;

  if (rank == 0) {
    if (argc != 5) {
      fprintf(stderr, "Usage: %s \
              APP_NPROCS APP_NNODES AGGR_NPROCS AGGR_NNODES\n", argv[0]);
      exit(-1);
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  n_app_procs = atoi(argv[1]);
  n_app_nodes = atoi(argv[2]);
  n_aggr_procs = atoi(argv[3]);
  n_aggr_nodes = atoi(argv[4]);
  if (rank == 0) {
    if (n_app_procs + n_aggr_procs != size) {
      fprintf(stderr, "Error: total nprocs is not equal to \
              application nprocs + aggregator nprocs\n");
      exit(-1);
    }
    if (n_app_procs % n_aggr_procs != 0) {
      fprintf(stderr, "Error: application nprocs is not \
              multiple of aggregator nprocs\n");
      exit(-1);
    }
  }

  if (rank < n_app_procs) {
    buf = (int *)malloc(MEGA * BUF_SIZE_IN_MB * sizeof(MPI_INT));
    memset(buf, 1, MEGA * BUF_SIZE_IN_MB * sizeof(MPI_INT));

    start = MPI_Wtime();
    dest_rank = n_app_procs + rank / (n_app_procs / n_aggr_procs);
    /*printf("Sending to process %3d\n", dest_rank);*/
    MPI_Send(buf, MEGA * BUF_SIZE_IN_MB, MPI_INT, dest_rank, 0, MPI_COMM_WORLD);
    if (buf) free(buf);
  } else {
    n_senders = n_app_procs / n_aggr_procs;
    buf = (int *)malloc(MEGA * BUF_SIZE_IN_MB * sizeof(MPI_INT) * n_senders);
    memset(buf, 1, MEGA * BUF_SIZE_IN_MB * sizeof(MPI_INT) * n_senders);

    start = MPI_Wtime();
    reqs = (MPI_Request *)malloc(n_senders * sizeof(MPI_Request));
    for (i = 0; i < n_senders; i++) {
      src_rank = (rank - n_app_procs) * n_senders + i;
      /*printf("Receiving from process %3d\n", src_rank);*/
      MPI_Irecv(buf + MEGA * BUF_SIZE_IN_MB * i,
                MEGA * BUF_SIZE_IN_MB, MPI_INT,
                src_rank, 0, MPI_COMM_WORLD,
                &reqs[i]);
    }
    MPI_Waitall(n_senders, reqs, MPI_STATUSES_IGNORE);
    if (reqs) free(reqs);
    if (buf) free(buf);
  }
  end = MPI_Wtime();
  duration = end - start;
  MPI_Reduce(&duration, &sum_duration, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    ave_duration = sum_duration / n_app_procs;
    printf("Time: %lf seconds, aggregate bandwidth is %lf MB/s\n",
           ave_duration,
           BUF_SIZE_IN_MB * sizeof(MPI_INT) / ave_duration * n_app_procs);
  }

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}
