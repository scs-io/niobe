/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 5/7/19.
//
#include <basket/hashmap/distributed_hash_map.h>
/* fixed size char for better inter process communications */

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);
    int comm_size,my_rank;
    MPI_Comm_size(MPI_COMM_WORLD,&comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    if (my_rank == 0) {
        sleep(2);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    typedef DistributedHashMap<CharStruct,CharStruct> distmap;
    int num_process_per_node=1, num_requests=0;
    if(argc < 3){
        perror("Insufficient args: usage: binary num_process_per_node num_requests");
    }
    if(argc > 1)  num_process_per_node=atoi(argv[1]);
    if(argc > 2)  num_requests=atoi(argv[2]);
    bool is_server = my_rank%num_process_per_node==0;
    int my_server = my_rank/num_process_per_node;
    int num_servers = comm_size/num_process_per_node;
    distmap map("METADATA_TEST",is_server,my_server,num_servers);
    Timer insertTimer=Timer();
    for(int i=0;i < num_requests;++i){
        insertTimer.resumeTime();
        map.Put(CharStruct(std::to_string(num_requests*my_rank+i)), CharStruct(std::to_string(i)));
        insertTimer.pauseTime();
        if (my_rank == 0) if (i % 200 == 0) printf("%d requests finished\n", i);
    }
    double insert_time_total = insertTimer.getElapsedTime();
    Timer queryTimer=Timer();
    for(int i=0;i < num_requests;++i){
        queryTimer.resumeTime();
        map.Get(CharStruct(std::to_string(num_requests*my_rank+i)));
        queryTimer.pauseTime();
    }
    double query_time_total = queryTimer.getElapsedTime();

    double ave_insert_time, ave_query_time, sum_insert_time, sum_query_time;
    if (my_rank == 0) printf("Collecting time information ...\n");
    MPI_Reduce(&insert_time_total, &sum_insert_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&query_time_total, &sum_query_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    if (my_rank == 0) {
      ave_insert_time = sum_insert_time / comm_size;
      ave_query_time = sum_query_time / comm_size;
      printf("insert_time: %f, query_time: %f\n", ave_insert_time, ave_query_time);
    }
    MPI_Finalize();
    return 0;
}
