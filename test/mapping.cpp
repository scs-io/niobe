/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by hariharan on 5/7/19.
//
#include <basket.h>
/* fixed size char for better inter process communications */

int main(int argc, char* argv[]){
    typedef DistributedHashMap<CharStruct,CharStruct> distmap;
    MPI_Init(&argc,&argv);
    int comm_size,my_rank;
    MPI_Comm_size(MPI_COMM_WORLD,&comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    int mode=1, num_requests=0;
    if(argc < 3){
        perror("Insufficient args: usage: binary num_process_per_node num_requests");
    }
    if(argc > 2)  mode=atoi(argv[1]);
    if(argc > 3)  num_requests=atoi(argv[2]);
    Timer t=Timer();
    if(mode==1){
        /**
         * F2O
         */
        for(int i=0;i < num_requests;++i){
            t.resumeTime();

            t.pauseTime();
        }
    } else{
        /**
         * O2F
         */
        for(int i=0;i < num_requests;++i){
            t.resumeTime();

            t.pauseTime();
        }
    }
    double time_total = t.getElapsedTime();
    printf("total_time: %f\n",time_total);
    return 0;
}
