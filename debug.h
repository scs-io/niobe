/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>

//#define DEBUG
/* Control whether debugging macros are active at compile time */
#undef DB_ACTIVE
#ifdef DEBUG
#define DB_ACTIVE 1
#else
#define DB_ACTIVE 0
#endif /* DEBUG */

/*
** Example use: KLUDGE("Fix macro to accept arguments with commas");
** Note that the argument is now a string.  An alternative (and
** previously used) design is to have the argument as a non-string:
**              KLUDGE(Fix macro to accept arguments with commas);
** This allows it to work with traditional compilers but runs foul of
** the absence of string concatenation, and you have to avoid commas
** in the reason string, etc.
**
** KLUDGE_FILE and FEATURE_FILE include the source file name after the
** main string.
**
** NB: If kludge.c (which defines kludge_use()) is compiled with
**     -DKLUDGE_VERBOSE, then the first time a feature or kludge is
**     used, a message is written to standard error.  The function
**     returns 1, so the reporting process is not repeated (for any
**     given feature or kludge).
*/

#define KLUDGE_ONCE   static int once = 0; if (once++ == 0) once =

#define KLUDGE(x)   { KLUDGE_ONCE kludge_use("@(#)KLUDGE: " x); }
#define FEATURE(x)  { KLUDGE_ONCE kludge_use("@(#)Feature: " x); }

#define KLUDGE_FILE(x)   KLUDGE(x " (" __FILE__ ")")
#define FEATURE_FILE(x)  FEATURE(x " (" __FILE__ ")")

/*
** TRACE is a legacy interface; new code should use DB_TRACE.
**
** Usage:  TRACE((level, fmt, ...));
**
** "level" is the debugging level which must be operational for the output
** to appear. "fmt" is a printf format string. "..." is whatever extra
** arguments fmt requires (possibly nothing).
**
** Usage:  DB_TRACE(level, fmt, ...);
** Usage:  DB_TRACELOC(level, fmt, ...);
**
** The structure of the macros means that the code is always validated
** but is not called when DEBUG is undefined.
** -- See chapter 8 of 'The Practice of Programming', by Kernighan and Pike.
*/
#define TRACE(x) \
              do { if (DB_ACTIVE) db_print x; } while (0)
#define DB_TRACE(level, ...)\
              do { if (DB_ACTIVE) db_print(level, __VA_ARGS__); } while (0)
#define DB_TRACELOC(level, ...)\
              do { if (DB_ACTIVE) db_printloc(level, __FILE__, __LINE__, __func__, __VA_ARGS__); } while (0)

/*
** DB_TRACKING(); uses the FEATURE_FILE macro from klduge.h to embed a
** string in a function identifying that the file is compiled with debug
** enabled.
*/
#define DB_TRACKING() \
              do { if (DB_ACTIVE) FEATURE_FILE("** DEBUGGING ENABLED **") } while (0)

enum DB_Options { DB_OPT_PID = 0x01 };     // Record PID at start of message

extern void     db_print(int level, const char *fmt, ...);
extern FILE    *db_getfileptr(void);

/* Semi-private function */
extern const char *db_indent(void);

extern int kludge_use(const char *str);

#endif /* DEBUG_H */
