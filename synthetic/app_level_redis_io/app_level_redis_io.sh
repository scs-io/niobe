#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

MPIEXEC_BIN=~/MPICH/bin/mpiexec
HOSTFILE=hosts
EXE_BIN=./app_level_redis_io
REDIS_DIR=~/pkg_src/redis-3.2.13
REDIS_SCRIPT_DIR=~/pkg_src/Utility-scripts/Redis
CWD=~/Integrator/synthetic/app_level_redis_io
SERVERS=servers
NODES=nodes
REDIS_PORT=7000
MODES=(1 0)
#REQ_SIZES=(32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072)
REQ_SIZES=(16384 32768 65536 131072)
REP=1

for req_size in ${REQ_SIZES[@]}
do
  for rep in `seq 1 ${REP}`
  do
    cd $REDIS_SCRIPT_DIR
    ./clean.sh
    ./start.sh
    cd $CWD
    for mode in ${MODES[@]}
    do
      echo Testing 1120 application procs on 28 nodes write to Redis, application request size ${req_size}KB ...
      echo ${MPIEXEC_BIN} -n 1120 -f ${HOSTFILE} -iface enp47s0 -prepend-rank ${EXE_BIN} ${mode} ${req_size}
      ${MPIEXEC_BIN} -n 1120 -f ${HOSTFILE} -iface enp47s0 -prepend-rank ${EXE_BIN} ${mode} ${req_size}
      if [[ "$mode" == 1 ]]
      then
        cd $REDIS_SCRIPT_DIR
        ./stop.sh
        ./start.sh
        cd $CWD
      fi
      echo Flushing page cache ...
      mpssh -f ${NODES} "sudo fm"
      echo Wait for 5 seconds ...
      sleep 5
    done
  done
done

