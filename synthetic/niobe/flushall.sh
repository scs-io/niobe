#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

HOSTFILE=hosts
REDIS_DIR=~/pkg_src/redis-3.2.13
SERVERS=servers
NODES=nodes
REDIS_PORT=7000

first_server=$(cat ${SERVERS} | head -1)
n_server=$(cat ${SERVERS} | wc -l)
server_host_prefix=$(echo ${first_server} | cut -d'-' -f1,2)
echo First server: ${first_server} out of ${n_server}
echo Server hostname perfix: ${server_host_prefix}
echo Flushing Redis masters ...
for i in 16 17 19 20 21 22 23 24 25 26 27 28 29 30 31 32
do
  i_n=$((10#${i}))
  if [[ "$i" > "18" ]]
  then
    ((i_n=$i_n-1))
  fi
  ((port=${i_n}+${REDIS_PORT}-16))
  echo Flushing ${server_host_prefix}-${i}:${port} ...
  echo flushall | ${REDIS_DIR}/src/redis-cli -c -h ${server_host_prefix}-${i} -p ${port}
done
echo Flushing Redis slaves ...
for i in 16 17 19 20 21 22 23 24 25 26 27 28 29 30 31 32
do
  i_n=$((10#${i}))
  if [[ "$i" > "18" ]]
  then
    ((i_n=$i_n-1))
  fi
  ((port=${i_n}+${REDIS_PORT}))
  echo Flushing ${server_host_prefix}-${i}:${port} ...
  echo flushall | ${REDIS_DIR}/src/redis-cli -c -h ${server_host_prefix}-${i} -p ${port}
done
