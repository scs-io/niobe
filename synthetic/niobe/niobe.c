/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <hircluster.h>

#define KILO            1024
#define MEGA            (1024 * 1024)

char *randstring(long length) {
  long n;
  static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.-#'?!";
  char *randomString = NULL;
  if (length) {
    randomString = (char *) malloc(sizeof(char) * (length + 1));
    if (randomString) {
      for (n = 0; n < length; n++) {
        int key = rand() % (int) (sizeof(charset) - 1);
        randomString[n] = charset[key];
      }
      randomString[length] = '\0';
    }
  }
  return randomString;
}

int main(int argc, char **argv) {
  MPI_Init(NULL, NULL);
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int mode = 0, req_size_in_kb = 0;
  int req_count = 0;

  if (argc != 3) {
    fprintf(stderr, "Usage: %s MODE REQ_SIZE_IN_KB\n", argv[0]);
    exit(-1);
  }
  mode = atoi(argv[1]);
  req_size_in_kb = atoi(argv[2]);

  redisClusterContext *cc;
  redisReply *reply;
  char nodelist[1024], key_str[1024];
  int i;
  double start, end, duration, sum_duration, ave_duration;

  /*sprintf(nodelist, "127.0.0.1:%d,127.0.0.1:%d,127.0.0.1:%d", port, port + 1, port + 2);*/
  /*strcpy(nodelist, "ares-stor-01:7000,ares-stor-02:7001,ares-stor-03:7002,ares-stor-04:7003,ares-stor-05:7004,ares-stor-06:7005,ares-stor-07:7006,ares-stor-08:7007,ares-stor-09:7008,ares-stor-10:7009,ares-stor-11:7010,ares-stor-12:7011,ares-stor-13:7012,ares-stor-14:7013,ares-stor-15:7014,ares-stor-16:7015");*/
  strcpy(nodelist, "ares-stor-15-40g:7000,ares-stor-16-40g:7001,ares-stor-19-40g:7002,ares-stor-20-40g:7003,ares-stor-21-40g:7004,ares-stor-22-40g:7005,ares-stor-23-40g:7006,ares-stor-24-40g:7007,ares-stor-25-40g:7008,ares-stor-26-40g:7009,ares-stor-27-40g:7010,ares-stor-28-40g:7011,ares-stor-29-40g:7012,ares-stor-30-40g:7013,ares-stor-31-40g:7014,ares-stor-32-40g:7015");
  /*strcpy(nodelist, "ares-comp-01:7000,ares-comp-01:7001,ares-comp-01:7002");*/
  cc = redisClusterContextInit();
  /*printf("Node list: %s\n", nodelist);*/
  redisClusterSetOptionAddNodes(cc, nodelist);
  redisClusterConnect2(cc);
  if (cc != NULL && cc->err) {
    printf("Error: %s\n", cc->errstr);
    // handle error
  }

  /* Set keys using binary safe API */
  if (req_size_in_kb > 8192) req_size_in_kb = 8192;
  req_count = 131072 / req_size_in_kb;
  req_size_in_kb *= 7;

  srand(1);
  const char *key_str_base = "niobe";
  char* buf = (char *)randstring(req_size_in_kb * KILO);

  start = MPI_Wtime();
  for (i = 0; i < req_count; i++) {
    memset(key_str, 0, 1024);
    sprintf(key_str, "%s_%d_%d", key_str_base, i, rank);
    if (mode == 1) {
      reply = redisClusterCommand(cc, "SET %s %b", key_str, buf, (size_t) req_size_in_kb * KILO);
      if (strcmp(reply->str, "OK"))
        printf("Something is wrong\n");
      freeReplyObject(reply);
    } else if (mode == 0) {
      reply = redisClusterCommand(cc, "GET %s", key_str);
      if (cc == NULL)
        printf("Something is wrong\n");
      freeReplyObject(reply);
    }
  }
  end = MPI_Wtime();
  duration = end - start;
  MPI_Reduce(&duration, &sum_duration, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    ave_duration = sum_duration / size;
    printf("Time: %lf seconds, aggregate bandwidth is %lf MB/s\n",
        ave_duration,
        req_size_in_kb * req_count * size / KILO / ave_duration);
  }

  /*if (pipeline_mode) {*/
    /*start = clock();*/
    /*for (i = 0; i < NUM_KEY; i++) {*/
      /*redisClusterGetReply(cc, (void **)&reply);*/
      /*if (strcmp(reply->str, "OK"))*/
        /*printf("Something is wrong\n");*/
      /*freeReplyObject(reply);*/
    /*}*/
    /*end = clock();*/
    /*duration = ((double) (end - start)) / CLOCKS_PER_SEC;*/
    /*printf("Time to get reply: %lf\n", duration);*/
  /*}*/

  /*[> Get keys <]*/
  /*start = clock();*/
  /*for (i = 0; i < NUM_KEY; i++) {*/
    /*memset(key_str, 0, 1024);*/
    /*sprintf(key_str, "%s_%d", key_str_base, i);*/
    /*printf("Getting key %s\n", key_str);*/
    /*if (pipeline_mode)*/
      /*redisClusterAppendCommand(cc, "GET %s", key_str);*/
    /*else {*/
      /*reply = redisClusterCommand(cc, "GET %s", key_str);*/
      /*[>printf("Reply: %s\n", reply->str);<]*/
      /*[>if (strcmp(reply->str, "OK"))<]*/
      /*[>printf("Something is wrong\n");<]*/
      /*freeReplyObject(reply);*/
    /*}*/
  /*}*/
  /*end = clock();*/
  /*duration = ((double) (end - start)) / CLOCKS_PER_SEC;*/
  /*printf("Time to get data: %lf\n", duration);*/

  /*if (pipeline_mode) {*/
    /*start = clock();*/
    /*for (i = 0; i < NUM_KEY; i++) {*/
      /*redisClusterGetReply(cc, (void **)&reply);*/
      /*[>printf("Reply: %s\n", reply->str);<]*/
      /*[>if (strcmp(reply->str, "OK"))<]*/
      /*[>printf("Something is wrong\n");<]*/
      /*freeReplyObject(reply);*/
    /*}*/
    /*end = clock();*/
    /*duration = ((double) (end - start)) / CLOCKS_PER_SEC;*/
    /*printf("Time to get reply: %lf\n", duration);*/
  /*}*/

  redisClusterReset(cc);

  /* Disconnects and frees the context */
  redisClusterFree(cc);

  if (buf) free(buf);
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();

  return 0;
}
