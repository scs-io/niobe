// Author: Wes Kendall
// Copyright 2011 www.mpitutorial.com
// This code is provided freely with the tutorials on mpitutorial.com. Feel
// free to modify it for your own use. Any distribution of the code must
// either provide a link to www.mpitutorial.com or keep this header intact.
//
// MPI_Send, MPI_Recv example. Communicates the number -1 from process 0
// to process 1.
//
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>

#define MEGA (1024 * 1024)
#define KILO 1024
#define BUF_SIZE_IN_MB 4
#define FILE_NAME_BASE "/mnt/nvme/kfeng/pvfs2-mount/data"

int main(int argc, char** argv) {
  MPI_Init(NULL, NULL);
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  int i;
  char *buf;
  double start, end, duration, sum_duration, ave_duration;
  MPI_File fh;
  MPI_Offset offset;
  int mode = 0, req_size_in_kb = 0, app_nprocs = 0;
  int req_count = 0, ratio = 0;

  if (rank == 0) {
    if (argc != 4) {
      fprintf(stderr, "Usage: %s MODE REQ_SIZE_IN_KB APP_NPROCS\n", argv[0]);
      exit(-1);
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  mode = atoi(argv[1]);
  req_size_in_kb = atoi(argv[2]);
  app_nprocs = atoi(argv[3]);

  ratio = app_nprocs / size; // must be 7
  req_count = 131072 / req_size_in_kb;
  buf = (char *)malloc(KILO * req_size_in_kb * req_count);
  memset(buf, 1, KILO * req_size_in_kb * req_count);

  start = MPI_Wtime();
  MPI_File_open(MPI_COMM_WORLD, FILE_NAME_BASE,
                MPI_MODE_RDWR | MPI_MODE_CREATE,
                MPI_INFO_NULL, &fh);
  offset = (MPI_Offset) req_size_in_kb * KILO * req_count * ratio * rank;
  if (mode == 0) {
    /*printf("Reading %d MPI_BYTE for %d times at offset %ld\n", req_size_in_kb * KILO, req_count, offset);*/
    for (i = 0; i < req_count; i++) {
      offset += req_size_in_kb * KILO * ratio;
      MPI_File_read_at(fh, offset,
                      buf, req_size_in_kb * KILO * ratio, MPI_BYTE,
                      MPI_STATUS_IGNORE);
    }
  } else if (mode == 1) {
    /*printf("Writing %d MPI_BYTE for %d times at offset %ld\n", req_size_in_kb * KILO, req_count, offset);*/
    for (i = 0; i < req_count; i++) {
      offset += req_size_in_kb * KILO * ratio;
      MPI_File_write_at(fh, offset,
                      buf, req_size_in_kb * KILO * ratio, MPI_BYTE,
                      MPI_STATUS_IGNORE);
    }
  }
  MPI_File_close(&fh);
  end = MPI_Wtime();
  duration = end - start;
  MPI_Reduce(&duration, &sum_duration, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    ave_duration = sum_duration / size;
    printf("Time: %lf seconds, aggregate bandwidth is %lf MB/s\n",
        ave_duration,
        req_size_in_kb * req_count * size * ratio / KILO / ave_duration);
  }

  if(buf) free(buf);
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}
