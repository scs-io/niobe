#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

MODES=(0)
#REQ_SIZES=(32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072)
REQ_SIZES=(32768 65536 131072)
CLIENTS=clients
SERVERS=servers
REP=1

for mode in ${MODES[@]}
do
  for req_size in ${REQ_SIZES[@]}
  do
    echo "Testing 160 aggregation procs running on 4 nodes write on behalf of 1120 application procs, application request size ${req_size}KB ..."
    for rep in `seq 1 $REP`
    do
      echo mpiexec -n 160 -prepend-rank -f hosts -iface enp47s0 ./baseline_file_io $mode $req_size 1120
      mpiexec -n 160 -prepend-rank -f hosts -iface enp47s0 ./baseline_file_io $mode $req_size 1120
      mpssh -f $CLIENTS 'sudo fm'
      mpssh -f $SERVERS 'sudo fm'
      sleep 2
    done
  done
done
