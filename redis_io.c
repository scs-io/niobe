/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include "debug.h"
#include "redis_io.h"

redisContext *redis_connect()
{
  struct timeval timeout = {REDIS_CONN_TIMEOUT_SEC, REDIS_CONN_TIMEOUT_USEC};
  redisContext *c = redisConnectWithTimeout(REDIS_HOST, REDIS_PORT, timeout);
  if (c == NULL || c->err) {
    if (c) {
      printf("Connection error: %s\n", c->errstr);
      redisFree(c);
    } else {
      printf("Connection error: can't allocate redis context\n");
    }
    exit(1);
  }
  return c;
}

void redis_write(redisContext *c, char *key, void *value, int size)
{
  redisReply *reply;
  DB_TRACE(2, "key: %s, length: %d\n", key, strlen(key));
  reply = redisCommand(c, "SET %s %b", key, value, (size_t)size);
  printf("SET (binary API): %s\n", reply->str);
  freeReplyObject(reply);
}

void redis_disconnect(redisContext *c)
{
  redisFree(c);
}
