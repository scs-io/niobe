/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef REDIS_IO_H
#define REDIS_IO_H

#include "hiredis.h"

#define REDIS_HOST "ares-stor-30"
#define REDIS_PORT 7000
#define REDIS_CONN_TIMEOUT_SEC 1
#define REDIS_CONN_TIMEOUT_USEC 500000

redisContext *redis_connect();
void redis_write(redisContext *c, char *key, void *value, int size);
void redis_disconnect(redisContext *c);

#endif
