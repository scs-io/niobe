/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef INTEGRATOR_H
#define INTEGRATOR_H

enum stor{POSIX = 0, REDIS = 1};

#define MEGA            (1024 * 1024)
//#define DEF_STOR        POSIX
#define DEF_STOR        REDIS
#define FILE_NAME_BASE  "data"
#define KEY_NAME_BASE   "data"
#define FILE_NAME_MAX   128
#define KEY_NAME_MAX    128

#endif
