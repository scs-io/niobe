/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include "debug.h"
/*#include "stderr.h"*/

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static int      debug = 2;

static char     blanks[] = "                                                 ";
static char    *indent = &blanks[sizeof(blanks) - 1];
/* GNU C library does not allow you to initialize debugfp to stderr */
static FILE    *debugfp = 0;
static int      options;

FILE *db_getfileptr(void)
{
  DB_TRACKING();
  if (debugfp == 0)
    debugfp = stderr;
  return debugfp;
}

/*
**  Call as: db_print(level, format, ...);
**  Print debug information if debug flag set at or above level.
*/
void db_print(int level, const char *fmt, ...)
{
  if (debug >= level)
  {
    va_list args;
    FILE *fp = db_getfileptr();
    fflush(stdout);
    flockfile(fp);
    fputs(db_indent(), fp);
    if (options & DB_OPT_PID)
      fprintf(fp, "%d: ", (int)getpid());
    va_start(args, fmt);
    vfprintf(fp, fmt, args);
    va_end(args);
    fflush(fp);
    funlockfile(fp);
  }
}

/*
** Return indent string
*/
const char *db_indent(void)
{
  return(indent);
}

/*
** The function should return 1 each time it is called, whether compiled
** with -DKLUDGE_VERBOSE or not.
**
** It is hard preventing modern compilers from optimizing away the
** recursive invocation implied by the FEATURE_FILE macro.  Using common
** built-ins like strlen() or strcmp() doesn't work, so for the time
** being, the code uses strxfrm() instead.  Since the result depends on
** the run-time locale, it can't easily be removed by the optimizer.
** The residual question is "what is its cost", which is hard to know.
** At worst, it gets called once per feature or kludge in the code per
** run, so the cost should be bearable.
*/

/* Possibly report on kludges used at run-time */
int kludge_use(const char *str)
{
  assert(str != 0 && *str != '\0');
#ifdef KLUDGE_VERBOSE
  FEATURE_FILE("KLUDGE Verbose");
  return fprintf(stderr, "%s\n", str) > 0;
#else
  FEATURE_FILE("KLUDGE Quiet");
  return strxfrm(0, str, 0) != 0;
#endif /* KLUDGE_VERBOSE */
}
