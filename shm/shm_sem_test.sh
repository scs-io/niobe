#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

num_of_shms=(1 2 4 8 10 16 20 32 40)
shm_sizes=(16 32 64 128 256)
xfer_sizes=(64 256 1024 4096 16384 65536)
num_of_rep=5

sed -i "s/\#define NUM_OF_REP.*/\#define NUM_OF_REP\\t\\t$num_of_rep/" common.h

for n_shm in ${num_of_shms[@]}
do
  sed -i "s/\#define NUM_OF_SHM.*/\#define NUM_OF_SHM\\t\\t$n_shm/" common.h
  for s_shm in ${shm_sizes[@]}
  do
    sed -i "s/\#define BUF_SIZE_IN_MB.*/\#define BUF_SIZE_IN_MB\\t$s_shm/" common.h
    for x_size in ${xfer_sizes[@]}
    do
      sed -i "s/\#define XFER_SIZE_IN_KB.*/\#define XFER_SIZE_IN_KB\\t\\t$x_size/" common.h
      echo "Testing $n_shm shms with size of ${s_shm}MB, transfer size ${xfer_sizes}KB ..."
      echo "Double checking configuration ..."
      conf_n_shm=`cat common.h | grep NUM_OF_SHM | awk '{print $3}'`
      conf_s_shm=`cat common.h | grep BUF_SIZE_IN_MB | awk '{print $3}'`
      conf_x_size=`cat common.h | grep XFER_SIZE_IN_KB | awk '{print $3}'`
      printf "number of shm: %s, size of shm: %sMB, transfer size: %sKB\n" $conf_n_shm $conf_s_shm $conf_x_size
      make clean && make -j > /dev/null
      ./writer_mmap
      mpiexec -n $n_shm ./reader_mmap
    done
  done
done
