/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef COMMON_H_
#define COMMON_H_

#define SHM_FILE_NAME         "shm_file"
#define FILL_SEM_FILE_NAME    "sem_fill"
#define EMPTY_SEM_FILE_NAME   "sem_empty"
#define PROJECT_ID            65
#define BUF_SIZE_IN_MB	      128
#define XFER_SIZE_IN_KB		    64
#define MEGA                  (1024 * 1024)
#define KILO                  1024
#define NUM_OF_SHM		        1
#define NUM_OF_REP		        10

unsigned char *md5sum(void *buf, long size);
double native_mem_test();

char *allocate_shm_sysv(char *shm_file, size_t size, int *shmid);
void release_shm_sysv(char *buf);
void destroy_shm_sysv(int shmid);

char *allocate_shm_mmap(char *shm_file, size_t size, int *fd);
void release_shm_mmap(char *buf, size_t size);
void destroy_shm_mmap(char *shm_file);

#endif
