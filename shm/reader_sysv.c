/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>
#include <mpi.h>
#include "common.h"

int main(int argc, char *argv[])
{
  char *buf;
  int shmid, rank;
  char shm_file[PATH_MAX];
  key_t key;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // ftok to generate unique key
  sprintf(shm_file, "%s_%d", SHM_FILE_NAME, rank);
  buf = allocate_shm_sysv(shm_file, BUF_SIZE_IN_MB * MEGA, &shmid);

  md5sum(buf, BUF_SIZE_IN_MB * MEGA);

  release_shm_sysv(buf);

  MPI_Finalize();

  return 0;
}
