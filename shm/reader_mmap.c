/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <mpi.h>
#include "common.h"

int main(int argc, char *argv[])
{
  char *shm_buf, *local_buf, temp;
  int i, shmid, rank, fd, id, rep;
  unsigned char *chksum;
  char shm_file[PATH_MAX], sem_file[PATH_MAX];
  double start, end, duration, sum_dura, max_dura;
  sem_t *sem_fill, *sem_empty;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  duration = 0;
  for (rep = 0; rep < NUM_OF_REP; rep++) {
    duration += native_mem_test();
  }
  sum_dura = 0;
  MPI_Reduce(&duration, &sum_dura, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    printf("Reader: average of %dx concurrent %dMB native memory: %f seconds\n",
            NUM_OF_SHM, BUF_SIZE_IN_MB, sum_dura / NUM_OF_SHM / NUM_OF_REP);
  }

  if (argc == 2)
    id = atoi(argv[1]);
  else
    id = rank;

  // ftok to generate unique key
  start = MPI_Wtime();
  sprintf(shm_file, "%s_%d", SHM_FILE_NAME, id);
  shm_buf = allocate_shm_mmap(shm_file, BUF_SIZE_IN_MB * MEGA, &fd);

  sprintf(sem_file, "%s_%d", FILL_SEM_FILE_NAME, id);
  if ((sem_fill = sem_open(sem_file, O_CREAT, 0644, 1)) == SEM_FAILED) {
    perror("Fail to initialize semaphore");
    exit(-1);
  }
  sprintf(sem_file, "%s_%d", EMPTY_SEM_FILE_NAME, id);
  if ((sem_empty = sem_open(sem_file, O_CREAT, 0644, 1)) == SEM_FAILED) {
    perror("Fail to initialize semaphore");
    exit(-1);
  }
  end = MPI_Wtime();
  duration = end - start;
  sum_dura = 0;
  MPI_Reduce(&duration, &sum_dura, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    printf("Reader: open %dx shared memory and semaphore: %f seconds\n",
            NUM_OF_SHM, sum_dura / NUM_OF_SHM);
  }

  duration = 0;
  MPI_Barrier(MPI_COMM_WORLD);
  local_buf = (char *)malloc(BUF_SIZE_IN_MB * MEGA);
  for (rep = 0; rep < NUM_OF_REP; rep++) {
    start = MPI_Wtime();
    for (i = 0; i < BUF_SIZE_IN_MB * KILO / XFER_SIZE_IN_KB; i++) {
      sem_wait(sem_empty);

      memcpy(local_buf + i * XFER_SIZE_IN_KB, shm_buf + i * XFER_SIZE_IN_KB,
             XFER_SIZE_IN_KB * KILO);

      sem_post(sem_fill);
    }
    end = MPI_Wtime();
    duration += end - start;

    for (i = 0; i < BUF_SIZE_IN_MB * MEGA; i++)
      temp = local_buf[i];
    chksum = md5sum(local_buf, BUF_SIZE_IN_MB * MEGA);
  }
  MPI_Barrier(MPI_COMM_WORLD);

  sum_dura = 0;
  MPI_Reduce(&duration, &max_dura, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    printf("Reader: average of %dx concurrent %dMB shared memory: %f seconds\n",
            NUM_OF_SHM, BUF_SIZE_IN_MB, max_dura / NUM_OF_REP);
  }

  release_shm_mmap(shm_buf, BUF_SIZE_IN_MB * MEGA);

  /*destroy_shm_mmap(shm_file);*/
  free(local_buf);

  MPI_Finalize();

  return 0;
}
