/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <openssl/md5.h>
#include <time.h>
#include "common.h"

/*****************************************************************************
 * MD5 functions
 ****************************************************************************/
unsigned char *md5sum(void *buf, long size)
{
  int i;
  unsigned char *hash;

  hash = (unsigned char *)malloc(MD5_DIGEST_LENGTH);
  MD5(buf, size, hash);
  printf("MD5: ");
  for (i = 0; i < MD5_DIGEST_LENGTH; i++)
    printf("%02X", hash[i]);
  printf("\n");
  return hash;
}

double native_mem_test()
{
  char *buf1, *buf2, *hash1, *hash2;
  int i;
  clock_t bigbang, start, end;
  double total_time, malloc_time, init_time, md5_time, memcpy_time, free_time;

  bigbang = clock();
  start = clock();
  buf1 = (char *)malloc(BUF_SIZE_IN_MB * MEGA);
  buf2 = (char *)malloc(BUF_SIZE_IN_MB * MEGA);
  end = clock();
  malloc_time = (end - start) / 1000000.0;
  printf("malloc time: %.10e seconds\n", malloc_time);

  start = clock();
  for (i = 0; i < BUF_SIZE_IN_MB * MEGA; i++)
    buf1[i] = i % 256;
  end = clock();
  init_time = (end - start) / 1000000.0;
  printf("init time: %.10e seconds\n", init_time);

  start = clock();
  buf1 = (char *)malloc(BUF_SIZE_IN_MB * MEGA);
  hash1 = md5sum(buf1, BUF_SIZE_IN_MB * MEGA);
  end = clock();
  md5_time = (end - start) / 1000000.0;

  start = clock();
  memcpy(buf2, buf1, BUF_SIZE_IN_MB * MEGA);
  end = clock();
  memcpy_time = (end - start) / 1000000.0;
  printf("memcpy time: %.10e seconds\n", memcpy_time);

  start = clock();
  hash2 = md5sum(buf2, BUF_SIZE_IN_MB * MEGA);
  end = clock();
  md5_time += (end - start) / 1000000.0;
  md5_time /= 2;
  printf("md5 time: %.10e seconds\n", md5_time);
  /*printf("Writing and checksuming a native %dMB memory block "*/
         /*"takes %f seconds\n", BUF_SIZE_IN_MB, end - start);*/
  start = clock();
  free(buf1);
  free(buf2);
  free(hash1);
  free(hash2);
  end = clock();
  free_time = (end - start) / 1000000.0;
  printf("free time: %.10e seconds\n", free_time);
  total_time = (end - bigbang) / 1000000.0;
  printf("total time: %.10e seconds\n", total_time);
  return memcpy_time;
}

/*****************************************************************************
 * System V version
 ****************************************************************************/
char *allocate_shm_sysv(char *shm_file, size_t size, int *shmid)
{
  char *buf;
  key_t key;

  // ftok to generate unique key
  if ((key = ftok(shm_file, PROJECT_ID)) == -1) {
    perror("Fail to generate key");
    exit(-1);
  }

  // shmget returns an identifier in shmid
  if ((*shmid = shmget(key, size, 0666 | IPC_CREAT)) == -1) {
    perror("Fail to get shm id");
    exit(-1);
  }

  // shmat to attach to shared memory
  if ((buf = (char *)shmat(*shmid, NULL, 0)) == (void *)-1) {
    perror("Fail to attach shm");
    exit(-1);
  }

  return buf;
}

void release_shm_sysv(char *buf)
{
  //detach from shared memory
  if ((shmdt(buf)) == -1) {
    perror("Fail to detach shm");
    exit(-1);
  }
}

void destroy_shm_sysv(int shmid)
{
  // destroy the shared memory
  shmctl(shmid, IPC_RMID, NULL);
}

/*****************************************************************************
 * mmap version
 ****************************************************************************/
char *allocate_shm_mmap(char *shm_file, size_t size, int *fd)
{
  char *buf;

  if ((*fd = shm_open(shm_file, O_RDWR | O_CREAT, 0644)) < 0) {
    printf("%s\n", shm_file);
    perror("Fail to open shm file");
    exit(-1);
  }
  assert(fd>0);

  ftruncate(*fd, size);

  if ((buf = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED,
                  *fd, 0)) == MAP_FAILED) {
    perror("Fail to map");
    exit(-1);
  }
  assert(buf);
  /*printf("Opened file %s on fd %d for mmap at address %p\n",*/
      /*shm_file, *fd, buf);*/

  return buf;
}

void release_shm_mmap(char *buf, size_t size)
{
  if (munmap(buf, size) == -1) {
    perror("Fail to unmap");
    exit(-1);
  }
}

void destroy_shm_mmap(char *shm_file)
{
  if (shm_unlink(shm_file) != 0) {
    perror("Fail to unlink");
    exit(-1);
  }
}

/*****************************************************************************
 * Semaphore
 ****************************************************************************/

