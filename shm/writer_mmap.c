/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <linux/limits.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <unistd.h>
#include <mpi.h>
#include "common.h"

int main(int argc, char *argv[])
{
  char *shm_buf[NUM_OF_SHM], *local_buf;
  unsigned char *chksums[NUM_OF_SHM];
  char shm_file[PATH_MAX], sem_file[PATH_MAX];
  int fds[NUM_OF_SHM], i, j, rep, s, ret;
  double start, end, duration;
  sem_t *sem_fill[NUM_OF_SHM], *sem_empty[NUM_OF_SHM];

  MPI_Init(&argc, &argv);
  srand(time(NULL));

  duration = 0;
  /*for (rep = 0; rep < NUM_OF_REP; rep++) {*/
    /*duration += native_mem_test();*/
  /*}*/
  /*printf("Writer: 1x %dMB native memory test: %f seconds\n",*/
          /*BUF_SIZE_IN_MB, duration / NUM_OF_REP);*/

  for (i = 0; i < NUM_OF_SHM; i++) {
    sprintf(shm_file, "%s_%d", SHM_FILE_NAME, i);
    shm_buf[i] = allocate_shm_mmap(shm_file, BUF_SIZE_IN_MB * MEGA, &fds[i]);

    sprintf(sem_file, "%s_%d", FILL_SEM_FILE_NAME, i);
    if ((sem_fill[i] = sem_open(sem_file, O_CREAT, 0644, 1)) == SEM_FAILED) {
      perror("Fail to create/open fill semaphore");
      exit(-1);
    }
    sprintf(sem_file, "%s_%d", EMPTY_SEM_FILE_NAME, i);
    if ((sem_empty[i] = sem_open(sem_file, O_CREAT, 0644, 1)) == SEM_FAILED) {
      perror("Fail to create/open empty semaphore");
      exit(-1);
    }
    if ((ret = sem_init(sem_fill[i], 1, 0)) != 0) {
      perror("Fail to init fill semaphore");
      exit(-1);
    }
    if ((ret = sem_init(sem_empty[i], 1, 1)) != 0) {
      perror("Fail to init empty semaphore");
      exit(-1);
    }
  }

  /*for (i = 0; i < NUM_OF_SHM; i++)*/
    /*sem_post(sem_fill[i]);*/

  local_buf = (char *)malloc(BUF_SIZE_IN_MB * MEGA);
  for (rep = 0; rep < NUM_OF_REP; rep++) {
    duration = 0;
    for (i = 0; i < NUM_OF_SHM; i++) {
      // Initialize buffer with kinda random data
      /*int r = rand();*/
      /*for (j = 0; j < BUF_SIZE_IN_MB * MEGA; j++)*/
        /*if (j % MEGA == 0) r = rand();*/
        /*local_buf[j] = (i + j) % 256 + r;*/

      start = MPI_Wtime();
      /*printf("Waiting for empty of SHM %d ...\n", i);*/
      sem_wait(sem_empty[i]);
      memcpy(shm_buf[i], local_buf, BUF_SIZE_IN_MB * MEGA);
      /*printf("Put data in SHM %d, notifying readers with fill semaphore ...\n", i);*/
      sem_post(sem_fill[i]);
      end = MPI_Wtime();
      duration += end - start;

      // Calculate MD5 checksum
      /*chksums[i] = md5sum(shm_buf[i], BUF_SIZE_IN_MB * MEGA);*/

      // Sleep
      /*s = (abs(rand()) % 1000) * 1000;*/
      /*printf("Writer: sleeping for %d ms ...\n", s / 1000);*/
      /*usleep(s);*/
    }
    printf("Writer: 1x %dMB shared memory test: %f seconds\n",
            BUF_SIZE_IN_MB, duration / NUM_OF_REP / NUM_OF_SHM);
  }

  for (i = 0; i < NUM_OF_SHM; i++) {
    if (chksums[i])
      free(chksums[i]);
    release_shm_mmap(shm_buf[i], BUF_SIZE_IN_MB * MEGA);
    sem_close(sem_fill[i]);
    sem_close(sem_empty[i]);
  }

  free(local_buf);
  MPI_Finalize();

  return 0;
}
