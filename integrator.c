/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "debug.h"
#include "integrator.h"
#include "posix_io.h"
#include "redis_io.h"

enum stor pick_best_stor()
{
  return DEF_STOR;
}

int main(int argc, char *argv[])
{
  size_t buf_size_in_mb;
  int rank, size, i, j, intgrt_rank, rep, nclient, nintgrt;
  void *buffer;
  double start, end, duration, dura_sum, dura_max;
  MPI_Win win;
  char host[32];
  int len;
  char fname[FILE_NAME_MAX], key[KEY_NAME_MAX];
  FILE *fp;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (argc != 4) {
    if (rank == 0)
      printf("Usage: mpiexec -n np %s BUF_SIZE REP NUM_AGGR_PROCS\n", argv[0]);
    MPI_Barrier(MPI_COMM_WORLD);
    exit(-1);
  }

  buf_size_in_mb = atoi(argv[1]);
  rep = atoi(argv[2]);
  nintgrt = atoi(argv[3]);
  nclient = size - nintgrt;

  MPI_Get_processor_name(host, &len);
  DB_TRACE(2, "running on %s\n", host);

  MPI_Barrier(MPI_COMM_WORLD);
  if (rank < nintgrt) {
    // integrator code
    MPI_Win_allocate(buf_size_in_mb * MEGA * nclient / nintgrt, 1,
                     MPI_INFO_NULL, MPI_COMM_WORLD, &buffer, &win);
    /*DB_TRACE(2, "win created\n");*/
    start = MPI_Wtime();
  } else {
    // application code
    MPI_Win_allocate(buf_size_in_mb * MEGA, 1,
                     MPI_INFO_NULL, MPI_COMM_WORLD, &buffer, &win);
    /*DB_TRACE(2, "win created\n");*/
    start = MPI_Wtime();
    intgrt_rank = rank % nintgrt;
    DB_TRACE(2, "putting data to integrator on P%02d\n", intgrt_rank);
    MPI_Win_lock(MPI_LOCK_SHARED, intgrt_rank, MPI_MODE_NOCHECK, win);
    for (j = 0; j < rep; j++) {
      MPI_Put(buffer, buf_size_in_mb * MEGA,
              MPI_BYTE, intgrt_rank,
              buf_size_in_mb * MEGA * (rank / nintgrt - 1), buf_size_in_mb * MEGA,
              MPI_BYTE, win);
      MPI_Win_flush(intgrt_rank, win);
    }
    MPI_Win_unlock(intgrt_rank, win);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  end = MPI_Wtime();
  duration = end - start;
  MPI_Reduce(&duration, &dura_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
  MPI_Reduce(&duration, &dura_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  if (rank == 0) {
    printf("Concurrent %dMB MPI_Put from %d ranks (%d repetitions)\n",
            (int)buf_size_in_mb, nclient, rep);
    printf("Time (wall time): %f seconds, aggregate bandwidth: %f GB/s\n",
            dura_max, buf_size_in_mb * nclient * rep / (dura_max * 1024));
    printf("Time (total time): %f seconds, aggregate bandwidth: %f GB/s\n",
            dura_sum, buf_size_in_mb * nclient * size * rep / (dura_sum * 1024));
  }

  start = MPI_Wtime();
  if (rank < nintgrt) {
    memset(fname, 0, FILE_NAME_MAX);
    strcpy(fname, FILE_NAME_BASE);
    sprintf(fname + strlen(fname), "%d", rank);
    if (pick_best_stor() == POSIX) {
      DB_TRACE(2, "opening file %s ...\n", fname);
      fp = posix_fopen(fname);
      if (fp) {
        posix_fwrite(buffer, buf_size_in_mb * MEGA * nclient / nintgrt, fp);
        fclose(fp);
      }
    } else if (pick_best_stor() == REDIS) {
      DB_TRACE(2, "connecting to Redis server %s:%d ...\n", REDIS_HOST, REDIS_PORT);
      redisContext *c = redis_connect();
      if (c) {
        memset(key, 0, KEY_NAME_MAX);
        strcpy(key, KEY_NAME_BASE);
        /*strcat(key, "hello");*/
        redis_write(c, key, buffer, buf_size_in_mb * MEGA * nclient / nintgrt);
        redis_disconnect(c);
      }
    }
  }
  end = MPI_Wtime();
  duration = end - start;
  MPI_Reduce(&duration, &dura_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    printf("Write %dMB from %d integrator processes\n",
            (int)buf_size_in_mb * nclient, nintgrt);
    printf("Time (wall time): %f seconds, aggregate bandwidth: %f GB/s\n",
            dura_max, buf_size_in_mb * nclient / (dura_max * 1024));
  }

  MPI_Win_free(&win);
  MPI_Finalize();

  return 0;
}
