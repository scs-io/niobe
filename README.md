# NIOBE

## Introduction
As we move forward to the exascale, accessing semantically different data from different storage resources is key to increasing productivity and to driving scientific discovery forward. To liberate data from their respective storage systems and provide transparent access to them we need to efficiently solve the integration between dominant data representations such as files in High Performance Computing (HPC) and objects in Big Data (BD) effectively providing an I/O bridge for complex workflows. The distinct representations of data introduce a semantic barrier between the two environments. At the same time, the power of each group of applications is needed by the application of the other side. Scientific workflows consisting of applications from both camps (i.e., HPC simulations and BD analysis) have emerged to make use of different advantages to solve scientific problems. How to exploit the existing resources and efficiently access data from both environments remains a challenge. Existing optimizations on compute and storage nodes ignore the I/O nodes on the I/O path. Decision making of integrated data access involves only the current operation and overlooks the data access cost of the life cycle of the data. We propose iNtelligent I/O Bridging Engine (NIOBE). NIOBE enables integrated data access for scientific workflows with asynchronous integrated I/O and data aggregation performed on I/O nodes. The data access is optimized to consider both the ongoing production and the consumption of the data in the future. With NIOBE, an integrated scientific workflow can be accelerated by up to 10.5x when compared with a no-integration baseline and can boost performance by up to 133% compared other state-of-the-art solutions.

## How it works
NIOBE intercepts I/O calls from application and select the target optimally based on workflow information
NIOBE utilizes the knowledge from IRIS to decide the optimal storage

## Usage
A collection of emulated tests for each component

## Dependencies
- IRIS
- Basket_orig

## Installation
`cd TEST_GROUP/TEST`
`make`
Where `TEST_GROUP` could be synthetic or workflow. `TEST` could be app_level_redis_io, baseline_file_io and niobe for synthetic, and aggr, copy_convert and flush for workflow.

## Running the tests
There is a shell script in each test directory to run all the tests to generate results. For example, you can test application-devel Redis performance using the following commands:
`cd synthetic/app_level_redis_io`
`./app_level_redis_io.sh >> app_level_redis_io.log`
The raw data will be available by running the following commands:
`cat app_level_redis_io.log | grep “aggregate bandwidth” | awk ‘{print $8}’`
The results are in a matrix of [#req_sizes, #rep, 2], you can average along the second dimension to get the average bandwidths.

## Results
[Performance of WRF workflow](https://bitbucket.org/scs-io/niobe/raw/c854e6149c0afd952ca8bfbbe750213d58bd9ba1/docs/WRF.png)
[Performance of CM1 workflow](https://bitbucket.org/scs-io/niobe/raw/c854e6149c0afd952ca8bfbbe750213d58bd9ba1/docs/CM1.png)

## Publication
K. Feng, H. Devarajan, A. Kougkas, and X.-H. Sun. "NIOBE: An Intelligent I/O Bridging Engine for Complex and Distributed Workflows,” IEEE International Conference on Big Data (accepted to appear), 2019.

