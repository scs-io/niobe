/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <vector>
#include <iomanip>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include "mpi.h"
#include "Timer.h"
#include "hircluster.h"

#define DEBUG 0
#define MAX_BUF_SIZE (4 * 1024 * 1024)
#define MAX_OBJ_SIZE (1 * 1024 * 1024)

redisClusterContext *cc;
std::string redis_node_base = "ares-stor";
std::string redis_node_postfix = "-40g";
int redis_port_base = 7000;
int redis_node_id_base = 1;
int redis_node_count = 16;

char *randstring(long length) {
  long n;
  static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.-#'?!";
  char *randomString = NULL;
  if (length) {
    randomString = (char *) malloc(sizeof(char) * (length + 1));
    if (randomString) {
      for (n = 0; n < length; n++) {
        int key = rand() % (int) (sizeof(charset) - 1);
        randomString[n] = charset[key];
      }
      randomString[length] = '\0';
    }
  }
  return randomString;
}

int replay_trace_aggr(std::string traceFile,
    int repetitions, int
    rank, int comm_size) {
  /*Initialization of some stuff*/
  FILE* trace;
  FILE* file = NULL;
  char* line = NULL;
  size_t len = 0;
  ssize_t readsize;
  std::string operation;
  long offset = 0;
  long request_size = 0;
  char* word;
  line = (char*) malloc(128);
  std::vector<double> timings;
  double average = 0;
  int rep =  repetitions;
  /* Do the I/O and comparison*/
  while (rep) {
    /*Opening the trace file*/
    trace = fopen(traceFile.c_str(), "r");
    if (trace == NULL) {
      std::cout << "Fail to open trace file "
                << traceFile
                << std::endl;
      return 0;
    }
    /*While loop to read each line from the trace and create I/O*/
    int lineNumber = 0;
    int app_nprocs = comm_size * 7 / 8;
    int aggr_nprocs = comm_size / 8;
    int app_nprocs_per_aggr = app_nprocs / aggr_nprocs;
    char *buf;
    if (rank < app_nprocs) {
      buf = (char *) malloc(MAX_BUF_SIZE);
      memset(buf, 1, MAX_BUF_SIZE);
    } else {
      buf = (char *) malloc(MAX_BUF_SIZE * app_nprocs_per_aggr);
      memset(buf, 1, MAX_BUF_SIZE * app_nprocs_per_aggr);
    }
    Timer globalTimer = Timer();
    globalTimer.startTime();
    time_t now = time(0);
    while ((readsize = getline(&line, &len, trace)) != -1) {
      if (readsize < 4) {
        break;
      }
      word = strtok(line, ",");
      operation = word;
      word = strtok(NULL, ",");
      offset = atol(word);
      word = strtok(NULL, ",");
      request_size = atol(word);
#if DEBUG == 1
      if (rank == 0)
        std::cout << "op: "
                  << operation
                  << ", offset: "
                  << offset
                  << ", size: "
                  << request_size
                  << std::endl;
#endif
      if (operation == "WRITE") {
        if (rank < app_nprocs) {
          int dest_rank = app_nprocs + rank / (app_nprocs / aggr_nprocs);
#if DEBUG == 1
          printf("Sending to process %3d\n", dest_rank);
#endif
          MPI_Send(buf, request_size, MPI_BYTE, dest_rank, 0, MPI_COMM_WORLD);
        } else {
          MPI_Request *reqs = (MPI_Request *)malloc(app_nprocs_per_aggr * sizeof(MPI_Request));
          for (int i = 0; i < app_nprocs_per_aggr; i++) {
            int src_rank = (rank - app_nprocs) * app_nprocs_per_aggr + i;
#if DEBUG == 1
            printf("Receiving from process %3d\n", src_rank);
#endif
            MPI_Irecv(buf + request_size * i,
                      request_size, MPI_BYTE,
                      src_rank, 0, MPI_COMM_WORLD,
                      &reqs[i]);
          }
          MPI_Waitall(app_nprocs_per_aggr, reqs, MPI_STATUSES_IGNORE);
          if (reqs) free(reqs);
        }
      } else if (operation == "READ") {
        if (rank < app_nprocs) {
          int src_rank = app_nprocs + rank / (app_nprocs / aggr_nprocs);
#if DEBUG == 1
          printf("Receiving from process %3d\n", src_rank);
#endif
          MPI_Recv(buf, request_size, MPI_BYTE, src_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        } else {
          MPI_Request *reqs = (MPI_Request *)malloc(app_nprocs_per_aggr * sizeof(MPI_Request));
          for (int i = 0; i < app_nprocs_per_aggr; i++) {
            int dest_rank = (rank - app_nprocs) * app_nprocs_per_aggr + i;
#if DEBUG == 1
            printf("Sending to process %3d\n", dest_rank);
#endif
            MPI_Isend(buf + request_size * i,
                      request_size, MPI_BYTE,
                      dest_rank, 0, MPI_COMM_WORLD,
                      &reqs[i]);
          }
          MPI_Waitall(app_nprocs_per_aggr, reqs, MPI_STATUSES_IGNORE);
          if (reqs) free(reqs);
        }
      }
      lineNumber++;
//      MPI_Barrier(MPI_COMM_WORLD);
    }
    //std::cout << "PFS,";
    timings.emplace_back(globalTimer.endTimeWithoutPrint(""));
    rep--;
    if (buf) free(buf);
    fclose(trace);
  }
  for (auto timing:timings){
    average += timing;
  }
  average = average / repetitions;
  double global_time;
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  MPI_Allreduce(&average, &global_time, 1, MPI_DOUBLE, MPI_SUM,
      MPI_COMM_WORLD);
  double mean = global_time / comm_size;

  if (rank == 0) {
    //printf("Time : %lf\n",mean);
    std::cout << traceFile
      << ",aggregation,"
      << std::setprecision(6)
      << std::to_string(mean)
      << "\n";
  }
  if (line) free(line);
  return 0;
}

/**
 * PFS code
 */
int prepare_data_pfs(std::string filename, int no_of_ranks) {
  std::string command="/home/kfeng/Integrator/CM1/replayer/preparepfsData.sh " +
                      filename + " " + std::to_string(no_of_ranks);
  system(command.c_str());
  return 0;
}

int replay_trace_pfs_io(std::string traceFile, std::string filename,
                      int repetitions, int
                      rank, int comm_size, int app_nprocs) {
  /*Initialization of some stuff*/
  FILE* trace;
  FILE* file = NULL;
  char* line = NULL;
  size_t len = 0;
  ssize_t readsize;
  std::string operation;
  long offset = 0;
  long request_size = 0;
  char* word;
  line = (char*) malloc(128);
  std::vector<double> timings;
  double average = 0;
  int rep =  repetitions;
  filename = filename + "_" + std::to_string(rank);
  /* Do the I/O and comparison*/
  while (rep) {
    /*Opening the trace file*/
    trace = fopen(traceFile.c_str(), "r");
    if (trace == NULL) {
      std::cout << "Fail to open trace file "
                << traceFile
                << std::endl;
      return 0;
    }
    /*While loop to read each line from the trace and create I/O*/
    int lineNumber = 0;
    int aggr_nprocs = comm_size;
    int app_nprocs_per_aggr = app_nprocs / aggr_nprocs;
    char* buf = (char*) malloc((size_t) MAX_BUF_SIZE * app_nprocs_per_aggr);
    Timer globalTimer = Timer();
    globalTimer.startTime();

    while ((readsize = getline(&line, &len, trace)) != -1) {
      if (readsize < 4) {
        break;
      }
      word = strtok(line, ",");
      operation = word;
      word = strtok(NULL, ",");
      offset = atol(word);
      word = strtok(NULL, ",");
      request_size = atol(word);
#if DEBUG == 1
        std::cout << "op: "
                  << operation
                  << ", offset: "
                  << offset
                  << ", size: "
                  << request_size
                  << std::endl;
#endif
      if (operation.find("OPEN") != std::string::npos) {
        for (int k = 0; k < 3; k++) {
          file = fopen(filename.c_str(), "w+");
          if (file != NULL) break;
          else std::cout << "Fail to open, retrying ..." << "\n";
        }
        if (file == NULL) {
          std::cerr << "Fail to open file "
                    << filename
                    << "\n";
          MPI_Abort(MPI_COMM_WORLD, -1);
        }
#if DEBUG == 1
        std::cout << filename
                  << " is opened"
                  << "\n";
#endif
      } else if (operation.find("CLOSE") != std::string::npos) {
        fclose(file);
#if DEBUG == 1
        std::cout << filename
                  << " is closed"
                  << "\n";
#endif
      } else if (operation == "WRITE") {
#if DEBUG == 1
        std::cout << "Writing "
                  << request_size * app_nprocs_per_aggr
                  << " bytes to "
                  << filename
                  << "\n";
#endif
        std::fseek(file, offset * app_nprocs_per_aggr, SEEK_SET);
        std::fwrite(buf, sizeof(char), request_size * app_nprocs_per_aggr, file);
      } else if (operation == "READ") {
#if DEBUG == 1
        std::cout << "Reading "
                  << request_size * app_nprocs_per_aggr
                  << " bytes from "
                  << filename
                  << "\n";
#endif
        std::fseek(file, offset * app_nprocs_per_aggr, SEEK_SET);
        std::fread(buf, sizeof(char), request_size * app_nprocs_per_aggr, file);
      } else if (operation == "LSEEK") {
        fseek(file, offset * app_nprocs_per_aggr, SEEK_SET);
      }
      lineNumber++;
//      MPI_Barrier(MPI_COMM_WORLD);
    }
    //std::cout << "PFS,";
    timings.emplace_back(globalTimer.endTimeWithoutPrint(""));
    rep--;
    if (buf) free(buf);
    fclose(trace);
  }
  for (auto timing:timings){
    average += timing;
  }
  average = average / repetitions;
  double global_time;
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  MPI_Allreduce(&average, &global_time, 1, MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
  double mean = global_time / comm_size;

  if (rank == 0) {
    //printf("Time : %lf\n",mean);
    std::cout << traceFile
              << ",pfs_io,"
              << std::setprecision(6)
              << std::to_string(mean)
              << "\n";
  }
  if (line) free(line);
  return 0;
}

/**
 * Redis code
 */
int redis_init(int rank)
{
  std::string nodelist;
  for (int i = 0; i < redis_node_count; i++) {
    std::string node_id_str = std::to_string(redis_node_id_base + i);
    nodelist += redis_node_base;
    nodelist += "-";
    nodelist += std::string(2 - node_id_str.length(), '0') + node_id_str + redis_node_postfix;
    nodelist += ":";
    nodelist += std::to_string(redis_port_base + i);
    if (i != redis_node_count - 1)
      nodelist += ",";
  }
  if (rank == 0)
    std::cout << "Redis node list: " << nodelist << std::endl;
  cc = redisClusterContextInit();
  redisClusterSetOptionAddNodes(cc, nodelist.c_str());
  redisClusterConnect2(cc);
  if (cc != NULL && cc->err) {
    printf("Fail to connect to Redis server: %s\n", cc->errstr);
    exit(-1);
  } else
    return 0;
}

int prepare_data_redis(std::string traceFile, int rank,
                       int app_nprocs, int aggr_nprocs, int optimized)
{
  /*Initialization of some stuff*/
  std::FILE* trace;
  char* line = NULL;
  size_t len = 0;
  ssize_t readsize;
  std::string operation;
  long offset = 0;
  long request_size = 0;
  char* word;
  line = (char*) malloc(128);
  redisReply *reply;

  /*Opening the trace file*/
  trace = std::fopen(traceFile.c_str(), "r");
  if (trace == NULL) {
    return -1;
  }

  /*While loop to read each line from the trace and create I/O*/
  int lineNumber = 0;
  int app_nprocs_per_aggr = app_nprocs / aggr_nprocs;

  while ((readsize = getline(&line, &len, trace)) != -1) {
    if (readsize < 4) {
      break;
    }
    word = strtok(line, ",");
    operation = word;
    word = strtok(NULL, ",");
    offset = atol(word);
    word = strtok(NULL, ",");
    request_size = atol(word);
    if (operation == "READ") {
      if(request_size == 0) continue;
      char* writebuf;
      if (request_size * app_nprocs_per_aggr > MAX_OBJ_SIZE && optimized == 0) {
        writebuf = randstring(MAX_OBJ_SIZE);
        int num_objects = (request_size * app_nprocs_per_aggr) / MAX_OBJ_SIZE + 1;
        for (int i = 0; i < num_objects; i++) {
          std::string key = std::to_string(offset) + "_" + std::to_string
                  (request_size) + "_" + std::to_string(rank) + "_" + std::to_string(i);
#if DEBUG == 1
          std::cout << "Putting key: " + key << std::endl;
#endif
          int obj_size = 0;
          if (i < num_objects - 1) obj_size = MAX_OBJ_SIZE;
          else obj_size = (request_size * app_nprocs_per_aggr) % MAX_OBJ_SIZE;
          reply = (redisReply *)
                  redisClusterCommand(cc, "SET %s %b", key.c_str(),
                                      writebuf,
                                      (size_t)(obj_size));
          if (strcmp(reply->str, "OK")) {
            printf("Something is wrong during preparation\n");
            exit(-1);
          }
          freeReplyObject(reply);
        }
      } else {
        writebuf = randstring(request_size * app_nprocs_per_aggr);
        std::string key = std::to_string(offset) + "_" + std::to_string
                (request_size) + "_" + std::to_string(rank);
#if DEBUG == 1
        std::cout << "Putting key: " + key << std::endl;
#endif
        reply = (redisReply *)
                redisClusterCommand(cc, "SET %s %b", key.c_str(),
                        writebuf,
                        (size_t)(request_size * app_nprocs_per_aggr));
        if (strcmp(reply->str, "OK")) {
          printf("Something is wrong during preparation\n");
          exit(-1);
        }
        freeReplyObject(reply);
      }

      if(writebuf) free(writebuf);
    }
    lineNumber++;
  }
  if (line) free(line);
  return 0;
}

int replay_trace_redis_io(std::string traceFile, int rank,
                          int app_nprocs, int aggr_nprocs, int optimized)
{
  /*Initialization of some stuff*/
  std::FILE* trace;
  char* line = NULL;
  int comm_size;
  size_t len=0;
  ssize_t readsize;
  std::string operation;
  long offset = 0;
  long request_size = 0;
  char* word;
  line = (char*) malloc(128);
  std::vector<double> timings;
  double average = 0;
  redisReply *reply;

  /*Opening the trace file*/
  trace = std::fopen(traceFile.c_str(), "r");
  if (trace == NULL) {
    return 0;
  }
  /*While loop to read each line from the trace and create I/O*/
  Timer globalTimer = Timer();
  globalTimer.startTime();
  int lineNumber=0;
  int app_nprocs_per_aggr = app_nprocs / aggr_nprocs;

  while ((readsize = getline(&line, &len, trace)) != -1) {
    if (readsize < 4) {
      break;
    }
    word = strtok(line, ",");
    operation = word;
    word = strtok(NULL, ",");
    offset = atol(word);
    word = strtok(NULL, ",");
    request_size = atol(word);
    if (operation == "WRITE") {
      if(request_size == 0) continue;
      char* writebuf;
      if (request_size * app_nprocs_per_aggr > MAX_OBJ_SIZE && optimized == 0) {
        writebuf = randstring(MAX_OBJ_SIZE);
        int num_objects = (request_size * app_nprocs_per_aggr) / MAX_OBJ_SIZE + 1;
        for (int i = 0; i < num_objects; i++) {
          std::string key = std::to_string(offset) + "_" + std::to_string
                  (request_size) + "_" + std::to_string(rank) + "_" + std::to_string(i);
#if DEBUG == 1
          std::cout << "Putting key: " + key << std::endl;
#endif
          int obj_size = 0;
          if (i < num_objects - 1) obj_size = MAX_OBJ_SIZE;
          else obj_size = (request_size * app_nprocs_per_aggr) % MAX_OBJ_SIZE;
          reply = (redisReply *)
                  redisClusterCommand(cc, "SET %s %b", key.c_str(),
                                      writebuf,
                                      (size_t)(obj_size));
          if (strcmp(reply->str, "OK")) {
            printf("Something is wrong during preparation\n");
            exit(-1);
          }
          freeReplyObject(reply);
        }
      } else {
        writebuf = randstring(request_size * app_nprocs_per_aggr);
        std::string key = std::to_string(offset) + "_" + std::to_string
                (request_size) + "_" + std::to_string(rank);
#if DEBUG == 1
        std::cout << "Putting key: " + key << std::endl;
#endif
        reply = (redisReply *)
                redisClusterCommand(cc, "SET %s %b", key.c_str(),
                                    writebuf,
                                    (size_t) (request_size * app_nprocs_per_aggr));
        if (strcmp(reply->str, "OK")) {
          printf("Something is wrong during replaying write\n");
          exit(-1);
        }
        freeReplyObject(reply);
      }
      if(writebuf) free(writebuf);
    } else if (operation == "READ") {
      if(request_size == 0) continue;
      if (request_size * app_nprocs_per_aggr > MAX_OBJ_SIZE && optimized == 0) {
        int num_objects = (request_size * app_nprocs_per_aggr) / MAX_OBJ_SIZE + 1;
        for (int i = 0; i < num_objects; i++) {
          std::string key = std::to_string(offset) + "_" + std::to_string
                  (request_size) + "_" + std::to_string(rank) + "_" + std::to_string(i);
#if DEBUG == 1
          std::cout << "Getting key: " + key << std::endl;
#endif
          int obj_size = 0;
          if (i < num_objects - 1) obj_size = MAX_OBJ_SIZE;
          else obj_size = (request_size * app_nprocs_per_aggr) % MAX_OBJ_SIZE;
          reply = (redisReply *) redisClusterCommand(cc, "GET %s", key.c_str());
          if (reply->len != obj_size) {
            printf("Something is wrong during replaying read\n");
            exit(-1);
          }
          freeReplyObject(reply);
        }
      } else {
        std::string key = std::to_string(offset) + "_" + std::to_string
                (request_size) + "_" + std::to_string(rank);
#if DEBUG == 1
        std::cout << "Getting key: " + key << std::endl;
#endif
        reply = (redisReply *) redisClusterCommand(cc, "GET %s", key.c_str());
        if (reply->len != request_size * app_nprocs_per_aggr) {
          printf("Something is wrong during read\n");
          exit(-1);
        }
        freeReplyObject(reply);
      }
    }
    lineNumber++;
  }
  timings.emplace_back(globalTimer.endTimeWithoutPrint(""));
  std::fclose(trace);
  for(auto timing:timings){
    average += timing;
  }
  double global_time;
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  MPI_Allreduce(&average, &global_time, 1, MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
  double mean = global_time / comm_size;

  if (rank == 0) {
    //printf("Time : %lf\n",mean);
    std::cout << traceFile
              << ",redis_io,"
              << std::setprecision(6)
              << std::to_string(mean)
              << "\n";
  }
  if (line) free(line);
  return 0;
}

void redis_finalize(int rank)
{
  return redisClusterFree(cc);
}

int main(int argc, char *argv[])
{
  int rank, comm_size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  if (comm_size % 8 != 0) {
    if (rank == 0) {
      std::cout << "Usage: "
        << argv[0]
        << " tracefile AGGR|PFS_IO|REDIS_IO|C&C|ANALYSIS_IO"
        << std::endl;
      std::cout << "Total process number needs to be a multiple of eight"
        << std::endl;
      MPI_Abort(MPI_COMM_WORLD, -1);
    }
  }
  std::string tracefile = argv[1];
  std::string phase = argv[2];

  if (phase == "AGGR") {
    replay_trace_aggr(tracefile, 1, rank, comm_size);
  } else if (phase == "PFS_IO") {
    std::string pathToFile = argv[3];
    int app_nprocs = atol(argv[4]);
    if (rank == 0 && argc < 6) prepare_data_pfs(pathToFile, comm_size);
    MPI_Barrier(MPI_COMM_WORLD);
    replay_trace_pfs_io(tracefile, pathToFile, 1, rank, comm_size, app_nprocs);
  } else if (phase == "REDIS_IO") {
    int app_nprocs = atol(argv[3]);
    int optimized = 1;
    redis_init(rank);
    prepare_data_redis(tracefile, rank, app_nprocs, comm_size, optimized);
    MPI_Barrier(MPI_COMM_WORLD);
    replay_trace_redis_io(tracefile, rank, app_nprocs, comm_size, optimized);
    redis_finalize(rank);
  } else if (phase == "REDIS_IO_CHUNK") {
    int app_nprocs = atol(argv[3]);
    int optimized = 0;
    redis_init(rank);
    prepare_data_redis(tracefile, rank, app_nprocs, comm_size, optimized);
    MPI_Barrier(MPI_COMM_WORLD);
    replay_trace_redis_io(tracefile, rank, app_nprocs, comm_size, optimized);
    redis_finalize(rank);
  }

  MPI_Finalize();
  return 0;
}
