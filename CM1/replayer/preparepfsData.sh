#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
# <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of NIOBE
# 
# NIOBE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
var=$2
rm /dev/shm/datafile &> /dev/shm/dd.log
echo Generating template file ...
dd if=/dev/zero of=/dev/shm/datafile bs=266M count=1 &> /dev/shm/dd.log
echo Deleting old files ...
rm -r ${1}* &> /dev/shm/dd.log
((niter=$var / 40))
echo Preparing $2 files in $niter rounds ...
((niter=$niter-1))
for iter in `seq 0 $niter`
do
    for i in `seq 1 40`
    do
        ((rank=$iter*40+$i-1))
        cmd="/opt/ohpc/pub/orangefs/bin/pvfs2-cp /dev/shm/datafile ${1}_$rank";
            #echo "${1}_${i} copied"
#        echo $cmd
        $cmd &
    done
#    sleep 3
    wait
done
echo Done
