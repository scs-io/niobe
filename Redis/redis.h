/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Kun Feng
 * <kfeng1@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
 *
 * This file is part of NIOBE
 * 
 * NIOBE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by kfeng on 4/18/19.
//

#ifndef REDIS_REDIS_H
#define REDIS_REDIS_H

#include <hircluster.h>

#define MAX_LINE_LEN      128
#define MAX_KEY_LEN       128
#define MAX_NODELIST_LEN  512

char *randstring(long length);

int redis_init(int rank);
int prepare_data_redis(char *trace_file, int rank);
int replay_trace_redis(char *trace_file, int rank);
void redis_finalize(int rank);

#endif //REDIS_REDIS_H
